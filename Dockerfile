
FROM ubuntu:14.04
env DEBIAN_FRONTEND=noninteractive


#add user
RUN groupadd -r calibre && useradd -g calibre -d /home/calibre -r -m calibre &&\
gpasswd -a calibre audio &&\
gpasswd -a calibre audio &&\
echo "root:calibre" | chpasswd &&\
echo "calibre:calibre" | chpasswd
 

ENV HOME /home/calibre
WORKDIR /tmp


ENV JAVA_HOME /usr/lib/jvm/java-8-oracle
ENV LD_LIBRARY_PATH /usr/local/lib
ENV PKG_CONFIG_PATH /usr/local/lib/pkgconfig

#install java
RUN  apt-get update -y -qq && \
	apt-get install  -y -qq git python xdg-utils imagemagick  libtool  swig  python-dev  bison  gcc  automake autoconf pkg-config \
	libpulse-dev make xz-utils alsa-utils   usbutils  && apt-get clean &&\
	apt-get install -y software-properties-common && \
	echo oracle-java8-installer shared/accepted-oracle-license-v1-1 select true | debconf-set-selections && \
	add-apt-repository -y ppa:webupd8team/java && \
	apt-get update && \
	apt-get install -y oracle-java8-installer && \
	rm -rf /var/lib/apt/lists/* && \
	rm -rf /var/cache/oracle-jdk8-installer &&\
	rm -rf ${JAVA_HOME}/man &&\
	rm -rf ${JAVA_HOME}/*src.zip \
           ${JAVA_HOME}/lib/missioncontrol \
           ${JAVA_HOME}/lib/visualvm \
           ${JAVA_HOME}/lib/*javafx* \
           ${JAVA_HOME}/jre/lib/plugin.jar \
           ${JAVA_HOME}/jre/lib/ext/jfxrt.jar \
           ${JAVA_HOME}/jre/bin/javaws \
           ${JAVA_HOME}/jre/lib/javaws.jar \
           ${JAVA_HOME}/jre/lib/desktop \
           ${JAVA_HOME}/jre/plugin \
           ${JAVA_HOME}/jre/lib/deploy* \
           ${JAVA_HOME}/jre/lib/*javafx* \
           ${JAVA_HOME}/jre/lib/*jfx* \
           ${JAVA_HOME}/jre/lib/amd64/libdecora_sse.so \
           ${JAVA_HOME}/jre/lib/amd64/libprism_*.so \
           ${JAVA_HOME}/jre/lib/amd64/libfxplugins.so \
           ${JAVA_HOME}/jre/lib/amd64/libglass.so \
           ${JAVA_HOME}/jre/lib/amd64/libgstreamer-lite.so \
           ${JAVA_HOME}/jre/lib/amd64/libjavafx*.so \
           ${JAVA_HOME}/jre/lib/amd64/libjfx*.so &&\
    rm -rf "$JAVA_HOME/jre/bin/jjs" \
           "$JAVA_HOME/jre/bin/keytool" \
           "$JAVA_HOME/jre/bin/orbd" \
           "$JAVA_HOME/jre/bin/pack200" \
           "$JAVA_HOME/jre/bin/policytool" \
           "$JAVA_HOME/jre/bin/rmid" \
           "$JAVA_HOME/jre/bin/rmiregistry" \
           "$JAVA_HOME/jre/bin/servertool" \
           "$JAVA_HOME/jre/bin/tnameserv" \
           "$JAVA_HOME/jre/bin/unpack200" \
           "$JAVA_HOME/jre/lib/ext/nashorn.jar" \
           "$JAVA_HOME/jre/lib/jfr.jar" \
           "$JAVA_HOME/jre/lib/jfr" &&\       
	mkdir -p  /home/calibre/ &&\
	mkdir -p "/home/calibre/Calibre Library/" &&\
	mkdir -p "/home/calibre/ila/" &&\
	mkdir -p "/home/calibre/pocketSphinxSource/" &&\
	git clone https://quickwritereader@bitbucket.org/quickwritereader/qwrdocker.git &&\
	tar xJf qwrdocker/calibre.tar.xz -C /home/calibre  &&\
	rm qwrdocker/calibre.tar.xz  &&\
	tar xJf qwrdocker/ila.tar.xz -C  /home/calibre/ila	&&\
	rm  qwrdocker/ila.tar.xz  &&\
	tar xJf qwrdocker/clibrary.tar.xz -C "/home/calibre/Calibre Library/" --strip-components=1 &&\
	rm qwrdocker/clibrary.tar.xz &&\ 
	tar xJf qwrdocker/pocketSphinxSource.tar.xz -C  /home/calibre/pocketSphinxSource --strip-components=1  &&\
	rm qwrdocker/pocketSphinxSource.tar.xz &&\
	rm -rf ./qwrdocker &&\
	cd /home/calibre/pocketSphinxSource/sphinxbase && ls && ./autogen.sh && make && make install &&\
	cd /home/calibre/pocketSphinxSource/pocketsphinx &&  ./autogen.sh && make && make install &&\
	apt-get remove -y -qq xz-utils git libtool  swig  python-dev  bison  gcc make  automake autoconf pkg-config  libpulse-dev &&\
	apt-get clean && apt-get clean autoclean &&\
	apt-get autoremove -y &&\
	rm -rf /var/lib/* &&\
	rm -rf /home/calibre/pocketSphinxSource &&\
 	chown -R calibre:calibre "/home/calibre" &&\
	chown  -R  calibre:calibre "/home/calibre/Calibre Library/"   &&\
	mkdir -p /home/calibre/data && chown  -R  calibre:calibre /home/calibre/data 

 
ENV PATH $PATH:/home/calibre/:/home/calibre/ila
WORKDIR /home/calibre/ila  
USER calibre
CMD   ./start.sh 

