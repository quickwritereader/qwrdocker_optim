## optimal image for  qwrdocker 
 Here we are going to decrease image size of https://bitbucket.org/quickwritereader/qwrdocker
#### For installing directly on Debian (tested only with Ubuntu) run installOnDebian.sh
See [installOnDebian.sh]( https://bitbucket.org/quickwritereader/qwrdocker/raw/b0ad739c07216d8ac6e316557a1a670bdb322e57/installOnDebian.sh )

### Version
0.0.1  
### The latest docker history of the built image 925.6 MB (ubuntu 188mb)
IMAGE |CREATED |CREATED BY |  SIZE|COMMENT 
:------|:-----|:------|:------|:------
11a0fa056c82 | 28 seconds ago | /bin/sh -c #(nop) CMD ["/bin/sh" "-c" "./star  | 0 B |  
  7ad0323e5613 | 28 seconds ago | /bin/sh -c #(nop) USER [calibre] | 0 B |  
  f8a68c314a30 | 29 seconds ago | /bin/sh -c #(nop) WORKDIR /home/calibre/ila    | 0 B |  
  21a74055af28 | 29 seconds ago | /bin/sh -c #(nop) ENV PATH=/usr/local/sbin:/u  | 0 B |  
  81622a583653 | 29 seconds ago | /bin/sh -c apt-get update -y -qq &&  apt-get |   737.3 MB |     
  ed52b4f648b8 | 2 hours ago |  /bin/sh -c #(nop) ENV PKG_CONFIG_PATH=/usr/lo |  0 B |  
  9889f0cf8e0f | 2 hours ago |  /bin/sh -c #(nop) ENV LD_LIBRARY_PATH=/usr/lo  | 0 B |  
  3772bf62dfe1 | 2 hours ago |  /bin/sh -c #(nop) ENV JAVA_HOME=/usr/lib/jvm/  | 0 B |  
  abb7ea2e6968 | 20 hours ago | /bin/sh -c #(nop) WORKDIR /tmp |   0 B |  
  64fbe8bd31aa | 20 hours ago | /bin/sh -c #(nop) ENV HOME=/home/calibre | 0 B |  
  cf90673a7b73 | 20 hours ago | /bin/sh -c groupadd -r calibre && useradd -g   | 333.7 kB |     
  ac44b47482d1 | 20 hours ago | /bin/sh -c #(nop) ENV DEBIAN_FRONTEND=noninte |  0 B |  
  b72889fa879c | 8 days ago |   /bin/sh -c #(nop) CMD ["/bin/bash"] |      0 B |  
  <missing> |    8 days ago |   /bin/sh -c sed -i 's/^#\s*\(deb.*universe\)$/  | 1.895 kB |     
  <missing> |    8 days ago |   /bin/sh -c set -xe   && echo '#!/bin/sh' > /u  | 194.5 kB |     
  <missing> |    8 days ago |   /bin/sh -c #(nop) ADD file:ed7184ebed5263e677  | 187.8 MB |